import { UserService } from './../services/user.service';
import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.sass']
})
export class ConversationComponent implements OnInit {
  friends: User[];
  friendId: any;
  friend: User;
  price:number = 78.3454353453534;
  today: any = Date.now();
  constructor(private activatedRoute: ActivatedRoute,
              private userServie: UserService
    ) { 

    this.friendId = this.activatedRoute.snapshot.params['uid'];
    console.log(this.friendId);
    this.friends = this.userServie.getFriends();
  
    this.friend = this.friends.find((record) => {
      return record.uid == this.friendId;
    });
    console.log(this.friend);
    
  }

  ngOnInit() {
  }

}

import { User } from './../interfaces/user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  friends: User[];
  constructor() {
    let myUser: User = {
      nick: 'jULIO',
      subnick: 'HOla!',
      age: 28,
      email: 'hola@frined.cow',
      friend:true,
      uid: 1,
      status: 'busy'

    };
    let myUser2: User = {
      nick: 'John',
      subnick: 'HOla!',
      age: 28,
      email: 'hola@frined.cow',
      friend:true,
      uid: 2,
      status: 'offline'

    };
    let myUser3: User = {
      nick: 'John',
      subnick: 'HOla!',
      age: 28,
      email: 'hola@frined.cow',
      friend:false,
      uid: 3,
      status: 'online'

    };
    let myUser4: User = {
      nick: 'John',
      subnick: 'HOla!',
      age: 28,
      email: 'hola@frined.cow',
      friend:false,
      uid: 4,
      status: 'away'

    };
    let users: User[] = [myUser,myUser2];
    console.log(users);
    this.friends = [myUser, myUser2,myUser3, myUser4]

    
   }
   getFriends(){
    return this.friends;
  }
}
